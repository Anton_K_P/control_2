package com.control_2;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "customer", uniqueConstraints = {@UniqueConstraint(columnNames = "id")})
public class CustomerPojo {

    private String age;
    private String name;
    private Integer sum;
    private Integer id;

    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Column(name = "name", length = 255, nullable = false)
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "sum", length = 255, nullable = false)
    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public String getName() {
        return name;
    }

    public Integer getSum() {
        return sum;
    }

    public String getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerPojo that = (CustomerPojo) o;
        return Objects.equals(age, that.age) && Objects.equals(name, that.name) && Objects.equals(sum, that.sum) && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name, sum, id);
    }
}

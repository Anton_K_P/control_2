package com.control_2;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.hibernate.Transaction;

import static com.control_2.DataBase.session;

@RestController
public class CustomerApi {

    CustomerPojo customerPojo = new CustomerPojo();

    @PostMapping(path = "/customer", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Integer id(@Validated  @RequestBody CustomerPojo pojo) {
        Transaction transaction = session.beginTransaction();
        customerPojo.setAge(pojo.getAge());
        customerPojo.setName(pojo.getName());
        customerPojo.setSum(pojo.getSum());
        session.save(customerPojo);
        transaction.commit();
        return customerPojo.getId();
    }

    @GetMapping(path = "/customer", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CustomerPojo getCustomerPojo(@RequestParam(required = false) Integer id) {
        return session.get(CustomerPojo.class, id);
    }

    @DeleteMapping(path = "/customer/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CustomerPojo deleteCustomerPojo(@PathVariable Integer id) {
        Transaction transaction = session.beginTransaction();
        CustomerPojo customer = getCustomerPojo(id);
        session.delete(customer);
        transaction.commit();
        return customer;
    }
}
